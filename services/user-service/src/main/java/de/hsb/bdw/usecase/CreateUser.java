package de.hsb.bdw.usecase;

import de.hsb.bdw.domain.model.UserDTO;
import de.hsb.bdw.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateUser {

    private final UserService userService;

    public UserDTO createOrUpdate(UserDTO userDTO) {
        return userService.createOrUpdateUser(userDTO);
    }
}
