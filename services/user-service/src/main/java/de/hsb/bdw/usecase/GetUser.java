package de.hsb.bdw.usecase;

import de.hsb.bdw.domain.model.UserDTO;
import de.hsb.bdw.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetUser {

    private final UserService userService;

    public UserDTO findById(String id) {
        return userService.findUserById(id);
    }
}
