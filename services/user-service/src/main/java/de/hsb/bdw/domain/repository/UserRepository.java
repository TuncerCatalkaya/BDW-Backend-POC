package de.hsb.bdw.domain.repository;

import de.hsb.bdw.domain.model.UserDTO;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository {

    UserDTO save(UserDTO userDTO);
    Optional<UserDTO> findUserById(String id);
}
