package de.hsb.bdw.domain.service;

import de.hsb.bdw.domain.model.UserDTO;
import de.hsb.bdw.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserDTO createOrUpdateUser(UserDTO userDTO) {
        return userRepository.save(userDTO);
    }

    public UserDTO findUserById(String id) {
        return userRepository.findUserById(id)
                .orElseThrow(UserNotFoundException::new);
    }
}
