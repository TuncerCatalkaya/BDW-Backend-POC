package de.hsb.bdw.jpa;

import de.hsb.bdw.domain.model.UserDTO;
import de.hsb.bdw.domain.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserRepositoryProxy implements UserRepository {

    private final JpaUserRepository jpaUserRepository;

    @Override
    @Transactional
    public UserDTO save(UserDTO userDTO) {
        return Optional.of(userDTO)
                .map(this::toEntity)
                .map(jpaUserRepository::save)
                .map(this::toDTO)
                .orElse(null);
    }

    @Override
    @Transactional
    public Optional<UserDTO> findUserById(String id) {
        return jpaUserRepository
                .findById(id)
                .map(this::toDTO);
    }

    private UserEntity toEntity(UserDTO userDTO) {
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(userDTO.getId());
        userEntity.setName(userDTO.getName());
        userEntity.setActive(userDTO.isActive());
        return userEntity;
    }

    private UserDTO toDTO(UserEntity userEntity) {
        return UserDTO.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .isActive(userEntity.isActive())
                .build();
    }
}
