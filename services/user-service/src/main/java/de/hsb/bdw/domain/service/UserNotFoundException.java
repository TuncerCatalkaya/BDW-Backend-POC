package de.hsb.bdw.domain.service;

import lombok.experimental.StandardException;

@StandardException
public class UserNotFoundException extends RuntimeException {
}
