package de.hsb.bdw.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ExceptionRestControllerAdviceTest {

    @InjectMocks
    private ExceptionRestControllerAdvice subject;

    @Test
    void testSomething() {
        assertNotNull(subject);
    }
}