package de.hsb.bdw.rest;

import de.hsb.bdw.domain.model.UserDTO;
import de.hsb.bdw.usecase.CreateUser;
import de.hsb.bdw.usecase.GetUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {

    private final CreateUser createUser;
    private final GetUser getUser;

    @PostMapping("/createOrUpdate")
    public UserDTO createOrUpdateUser(@RequestBody UserDTO userDTO) {
        return createUser.createOrUpdate(userDTO);
    }

    @GetMapping
    public UserDTO findUserById(@RequestParam String id) {
        return getUser.findById(id);
    }
}
