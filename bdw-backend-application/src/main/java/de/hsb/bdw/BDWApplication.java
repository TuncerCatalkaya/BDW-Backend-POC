package de.hsb.bdw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BDWApplication {

    public static void main(String[] args) {
        SpringApplication.run(BDWApplication.class, args);
    }
}
